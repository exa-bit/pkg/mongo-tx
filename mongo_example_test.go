package mongotx_test

import (
	"context"
	"gitlab.com/exa-bit/pkg/mongo-tx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
)

func ExampleMongoInTransaction() {
	var db *mongo.Database // assume db is valid

	err := mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection("posts").InsertOne(sc, bson.M{"title": "My post"})
		if err != nil {
			return err
		}
		postId := res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection("comments").InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		if err != nil {
			return err // return err will rollback transaction
		}
		return nil // return nil (no error) will commit transaction
	})
	log.Fatal("cannot start transaction!", err)
}
