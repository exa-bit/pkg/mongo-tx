package mongotx_test

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/exa-bit/pkg/mongo-tx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"testing"
)

func try(err error) {
	if err != nil {
		panic(err)
	}
}

var (
	postCollection    = "posts"
	commentCollection = "comments"
)

func dropMongoTestDB(db *mongo.Database) {
	try(db.Drop(context.Background()))
}

// drop db to clean it up, then recreate the database and collections
func prepareMongoTestEnvironment(db *mongo.Database) {
	dropMongoTestDB(db)
	try(db.CreateCollection(context.Background(), postCollection))
	try(db.CreateCollection(context.Background(), commentCollection))
}

// test atomic commit multi document
func TestMongoInTransaction_AtomicCommit(t *testing.T) {
	var dbName = "mongoInTransaction"
	var client, err = mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}
	if err = client.Connect(context.TODO()); err != nil {
		panic(err)
	}
	var db = client.Database(dbName)
	defer dropMongoTestDB(db)

	prepareMongoTestEnvironment(db)
	var postId primitive.ObjectID
	err = mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection(postCollection).InsertOne(sc, bson.M{"title": "My post"})
		try(err)
		postId = res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection(commentCollection).InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		try(err)
		return nil
	})
	if assert.NoError(t, err) {
		c, err := db.Collection(postCollection).CountDocuments(context.TODO(), bson.M{})
		try(err)
		assert.Equal(t, 1, int(c))
		c, err = db.Collection(postCollection).CountDocuments(context.TODO(), bson.M{})
		try(err)
		assert.Equal(t, 1, int(c))
		c, err = db.Collection(commentCollection).CountDocuments(context.TODO(), bson.D{{"post_id", postId}})
		try(err)
		assert.Equal(t, 1, int(c))
	}
}

// test atomic rollback multi document
func TestMongoInTransaction_AtomicRollback(t *testing.T) {
	var dbName = "mongoInTransaction"
	var client, err = mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}
	if err = client.Connect(context.TODO()); err != nil {
		panic(err)
	}
	var db = client.Database(dbName)
	defer dropMongoTestDB(db)

	prepareMongoTestEnvironment(db)
	var postId primitive.ObjectID
	err = mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection(postCollection).InsertOne(sc, bson.M{"title": "My post"})
		try(err)
		postId = res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection(commentCollection).InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		try(err)
		return errors.New("should rollback")
	})
	if assert.NoError(t, err) {
		c, err := db.Collection(postCollection).CountDocuments(context.TODO(), bson.M{})
		try(err)
		assert.Equal(t, 0, int(c))
		c, err = db.Collection(postCollection).CountDocuments(context.TODO(), bson.M{})
		try(err)
		assert.Equal(t, 0, int(c))
		c, err = db.Collection(commentCollection).CountDocuments(context.TODO(), bson.D{{"post_id", postId}})
		try(err)
		assert.Equal(t, 0, int(c))
	}
}

// test two nested (separate) transactions, the inner session should committed while the outer session should rollback.
func TestMongoInTransaction_NestedSessions(t *testing.T) {
	var dbName = "mongoInTransaction"
	var client, err = mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}
	if err = client.Connect(context.TODO()); err != nil {
		panic(err)
	}
	var db = client.Database(dbName)
	defer dropMongoTestDB(db)

	prepareMongoTestEnvironment(db)
	err = mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection(postCollection).InsertOne(sc, bson.M{"title": "My post"})
		try(err)
		postId := res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection(commentCollection).InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		try(err)

		err = mongotx.MongoInTransaction(context.TODO(), db, func(sc2 context.Context) error {
			res, err := db.Collection(postCollection).InsertOne(sc2, bson.M{"title": "My post 2"})
			try(err)
			postId2 := res.InsertedID.(primitive.ObjectID)
			res, err = db.Collection(commentCollection).InsertOne(sc2, bson.M{"review": "My review 2", "post_id": postId2})
			try(err)
			return nil
		})
		assert.NoError(t, err)

		return errors.New("should rollback")
	})
	if assert.NoError(t, err) {
		cursor, err := db.Collection(postCollection).Find(context.TODO(), bson.M{})
		try(err)
		var docs []bson.M
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 1)
		assert.Equal(t, "My post 2", docs[0]["title"])

		cursor, err = db.Collection(commentCollection).Find(context.TODO(), bson.M{})
		try(err)
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 1)
		assert.Equal(t, "My review 2", docs[0]["review"])
	}
}

// test atomic commit of two nested (as single transaction) transactions
func TestMongoInTransaction_NestedSessions_Reused_AtomicCommit(t *testing.T) {
	var dbName = "mongoInTransaction"
	var client, err = mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}
	if err = client.Connect(context.TODO()); err != nil {
		panic(err)
	}
	var db = client.Database(dbName)
	defer dropMongoTestDB(db)

	prepareMongoTestEnvironment(db)
	err = mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection(postCollection).InsertOne(sc, bson.M{"title": "My post"})
		try(err)
		postId := res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection(commentCollection).InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		try(err)

		err = mongotx.MongoInTransaction(sc, db, func(sc2 context.Context) error {
			res, err := db.Collection(postCollection).InsertOne(sc2, bson.M{"title": "My post 2"})
			try(err)
			postId2 := res.InsertedID.(primitive.ObjectID)
			res, err = db.Collection(commentCollection).InsertOne(sc2, bson.M{"review": "My review 2", "post_id": postId2})
			try(err)
			return nil
		})
		assert.NoError(t, err)

		return nil
	})
	if assert.NoError(t, err) {
		cursor, err := db.Collection(postCollection).Find(context.TODO(), bson.M{})
		try(err)
		var docs []bson.M
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 2)
		assert.Equal(t, "My post", docs[0]["title"])

		cursor, err = db.Collection(commentCollection).Find(context.TODO(), bson.M{})
		try(err)
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 2)
		assert.Equal(t, "My review", docs[0]["review"])
	}
}

// test atomic rollback of two nested (as single transaction) transactions
func TestMongoInTransaction_NestedSessions_Reused_AtomicRollback(t *testing.T) {
	var dbName = "mongoInTransaction"
	var client, err = mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}
	if err = client.Connect(context.TODO()); err != nil {
		panic(err)
	}
	var db = client.Database(dbName)
	defer dropMongoTestDB(db)

	prepareMongoTestEnvironment(db)
	err = mongotx.MongoInTransaction(context.TODO(), db, func(sc context.Context) error {
		res, err := db.Collection(postCollection).InsertOne(sc, bson.M{"title": "My post"})
		try(err)
		postId := res.InsertedID.(primitive.ObjectID)
		res, err = db.Collection(commentCollection).InsertOne(sc, bson.M{"review": "My review", "post_id": postId})
		try(err)

		err = mongotx.MongoInTransaction(sc, db, func(sc2 context.Context) error {
			res, err := db.Collection(postCollection).InsertOne(sc2, bson.M{"title": "My post 2"})
			try(err)
			postId2 := res.InsertedID.(primitive.ObjectID)
			res, err = db.Collection(commentCollection).InsertOne(sc2, bson.M{"review": "My review 2", "post_id": postId2})
			try(err)
			return nil
		})
		assert.NoError(t, err)

		return errors.New("should rollback")
	})
	if assert.NoError(t, err) {
		cursor, err := db.Collection(postCollection).Find(context.TODO(), bson.M{})
		try(err)
		var docs []bson.M
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 0)

		cursor, err = db.Collection(commentCollection).Find(context.TODO(), bson.M{})
		try(err)
		try(cursor.All(context.TODO(), &docs))
		assert.Len(t, docs, 0)
	}
}