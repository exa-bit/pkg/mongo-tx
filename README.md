# exa-bit/pkg/mongo-tx

Contains simplified closure based function to create multi documents transaction.
The aim is to provide similar function signature to work with mongo transaction as with another database (ie. sql and pgx).

## Tested On
- mongodb v4.2 - `go.mongodb.org/mongo-driver v1.4.0-rc0`
