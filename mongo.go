package mongotx

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*
MongoInTransaction creates and starts a new Session and use it to call the fn callback.
The Context try be used as the Context parameter for any operations in the fn callback that should be executed under the session.
If the ctx parameter already contains a Session, that Session will be reused instead effectively become 1 transaction.
To create new transaction instead of reused transaction, supply different ctx which doesn't contains any `mongo.Session`.
Any error returned by the fn callback will abort (rollback) the transaction, otherwise commit it.
An error will be returned if there is error when performing commit or abort operation.
*/
func MongoInTransaction(ctx context.Context, db *mongo.Database, fn func(sc context.Context) error, opts ...*options.TransactionOptions) error {

	// if session is not nil, continue to use existing session/transaction
	if sess := mongo.SessionFromContext(ctx); sess != nil {
		return fn(ctx)
	}

	sess, err := db.Client().StartSession()
	if err != nil {
		return err
	}

	return mongo.WithSession(ctx, sess, func(sc mongo.SessionContext) error {
		defer sess.EndSession(context.Background())

		if err := sc.StartTransaction(opts...); err != nil {
			return err
		}

		if err := fn(sc); err != nil {
			return sc.AbortTransaction(sc)
		}

		return sc.CommitTransaction(sc)
	})
}
